## Installation

$ composer install

Lancer le projet:

$ sudo docker-compose up

## Utilisation

Symfony: "localhost:2000"
React: "localhost:3000"
Adminer: "localhost:8080"

server name: "project-devops-mariadb"
user: "root"
password: "root"
database: "project-devops"

## Recommandation

